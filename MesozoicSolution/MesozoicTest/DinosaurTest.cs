﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using Mesozoic;

namespace MesozoicTest
{
    [TestClass]
    public class DinosaurTest
    {
        [TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.name);
            Assert.AreEqual("Stegausaurus", louis.specie);
            Assert.AreEqual(12, louis.age);
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.Roar());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Je me présente, je m'appelle Louis le Stegausaurus, j'ai 12 ans et je voudrais réussir ma vie.", louis.SayHello());
        }

        [TestMethod]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.AreEqual("Je suis Louis et je fais un calin à Nessie.", louis.Hug(nessie));
        }

    }
}
