﻿using System;
using System.Collections.Generic;
namespace Mesozoic
{
    public class Dinosaur
    {
        public string name;
        public string specie;
        public int age;


        public Dinosaur(string name, string specie, int age)
        {
            this.name = name;
            this.specie = specie;
            this.age = age;
        }
        
        public string SayHello()
        {
            return string.Format("Je me présente, je m'appelle {0} le {1}, j'ai {2} ans et je voudrais réussir ma vie.",this.name, this.specie, this.age);
        }

        public string Roar()
        {
            return "Grrr";
        }

        public string Hug(Dinosaur dinosaur)
        {
            return string.Format("Je suis {0} et je fais un calin à {1}.", this.name, dinosaur.name);
        }

    }

}