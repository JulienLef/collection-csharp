﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicConsole
{
    class Program
    {
        static string GetName(Mesozoic.Dinosaur dinosaur)
        {
            return dinosaur.name;
        }

        static string GetSpecie(Mesozoic.Dinosaur dinosaur)
        {
            return dinosaur.specie;
        }

        static int GetAge(Mesozoic.Dinosaur dinosaur)
        {
            return dinosaur.age;
        }

        static string SetName()
        {
            Console.Write("Donner son nom : ");
            string nom = Console.ReadLine();
            return nom;
        }

        static string SetSpecie()
        {
            Console.Write("Donner son espece : ");
            string specie = Console.ReadLine();
            return specie;
        }

        static int SetAge()
        {
            int age;
            string age_string;
            do
            {
                Console.Write("Donner son age : ");
                age_string = Console.ReadLine();
            } while (!int.TryParse(age_string, out age));
            return age;
        }

        static void Main(string[] args)
        {
            /*Console.WriteLine("Donner naissance à un dinosaure : ");
            Mesozoic.Dinosaur dinosaur1 = new Mesozoic.Dinosaur("", "", 0);
            dinosaur1.name = setName();
            dinosaur1.specie = setSpecie();
            dinosaur1.age = setAge();

            Console.WriteLine(dinosaur1.hug(dinosaur1));
            Console.WriteLine(dinosaur1.SayHello());
            Console.WriteLine(dinosaur1.Roar());
            }*/

            Mesozoic.Dinosaur henri = new Mesozoic.Dinosaur("Henri", "Stegausaurus", 12);
            Mesozoic.Dinosaur nessie = new Mesozoic.Dinosaur("Nessie", "Diplodocus", 11);
            Mesozoic.Dinosaur laurent = new Mesozoic.Dinosaur("Laurent", "Stratopercus", 13);
            Mesozoic.Dinosaur jade = new Mesozoic.Dinosaur("Jade", "Trouducus", 10);

            List<Mesozoic.Dinosaur> dinosaurs = new List<Mesozoic.Dinosaur>();

            dinosaurs.Add(henri); //Append dinosaur reference to end of list
            dinosaurs.Add(nessie);
            dinosaurs.Add(laurent);
            dinosaurs.Add(jade);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Mesozoic.Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.SayHello());
            }

            dinosaurs.RemoveAt(1); //Remove dinosaur at index 1

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Mesozoic.Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.SayHello());
            }

            dinosaurs.Remove(henri);

            Console.WriteLine(dinosaurs.Count);
            //Iterate over our list
            foreach (Mesozoic.Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.SayHello());
            }

            Console.ReadKey();
        }
    }
}
